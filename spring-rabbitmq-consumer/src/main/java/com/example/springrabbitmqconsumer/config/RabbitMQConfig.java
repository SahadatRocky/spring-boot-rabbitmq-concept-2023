package com.example.springrabbitmqconsumer.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    private static final String ROUTING_A = "routing.A";
    private static final String ROUTING_B = "routing.B";

    @Bean
    public Queue queueA() {
        return new Queue("queue.A", false);
    }

    @Bean
    public Queue queueB() {
        return new Queue("queue.B", false);
    }

    @Bean
    public Queue queueAll() {
        return new Queue("queue.All", false);
    }


    //Direct exchange

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("exchange.direct");
    }

    @Bean
    public Binding binding(Queue queueA, DirectExchange directExchange) {
        return BindingBuilder.bind(queueA)
                .to(directExchange)
                .with(ROUTING_A);
    }

    @Bean
    public Binding bindingB(Queue queueB, DirectExchange directExchange) {
        return BindingBuilder.bind(queueB)
                .to(directExchange)
                .with(ROUTING_B);
    }


    /*
    //fanout exchnage

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("exchange.fanout");
    }

    @Bean
    public Binding binding(Queue queueA, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queueA)
                .to(fanoutExchange);
    }

    @Bean
    public Binding bindingB(Queue queueB, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queueB)
                .to(fanoutExchange);
    }

     */

    /*
       // topic exchange




    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange("exchange.topic");
    }

    @Bean
    public Binding binding(Queue queueA, TopicExchange topicExchange) {
        return BindingBuilder.bind(queueA)
                .to(topicExchange)
                .with(ROUTING_A);
    }

    @Bean
    public Binding bindingB(Queue queueB, TopicExchange topicExchange) {
        return BindingBuilder.bind(queueB)
                .to(topicExchange)
                .with(ROUTING_B);
    }

    @Bean
    public Binding bindingAll(Queue queueAll, TopicExchange topicExchange) {
        return BindingBuilder.bind(queueAll)
                .to(topicExchange)
                .with("routing.*");
    }

     */

    /*
       // Header exchange



    @Bean
    public HeadersExchange headersExchange() {
        return new HeadersExchange("exchange.header");
    }

    @Bean
    public Binding binding(Queue queueA, HeadersExchange headersExchange) {
        return BindingBuilder.bind(queueA)
                .to(headersExchange)
                .where("color")
                .matches("red");
    }

    @Bean
    public Binding bindingB(Queue queueB, HeadersExchange headersExchange) {
        return BindingBuilder.bind(queueB)
                .to(headersExchange)
                .where("color")
                .matches("blue");
    }

    @Bean
    public Binding bindingAll(Queue queueAll, HeadersExchange headersExchange) {
        return BindingBuilder.bind(queueAll)
                .to(headersExchange)
                .where("color")
                .matches("green");
    }

     */

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory factory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(factory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }
}