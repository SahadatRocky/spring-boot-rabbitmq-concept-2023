package com.example.springrabbitmqconsumer.controller;

import com.example.springrabbitmqconsumer.model.MyMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Consumer {

    @RabbitListener(queues = "queue.A")
    private void receive(MyMessage message){
       log.info("Message receive from queue A -> {}", message);
    }

    @RabbitListener(queues = "queue.B")
    private void receiveFromB(MyMessage message){
        log.info("Message receive from queue B -> {}", message);
    }


    @RabbitListener(queues = "queue.All")
    private void receiveFromAll(MyMessage message){
        log.info("Message receive from queue All -> {}", message);
    }


    /*
       Header exchange


    @RabbitListener(queues = "queue.A")
    private void receive(String message){
        log.info("Message receive from queue A -> {}", message);
    }

    @RabbitListener(queues = "queue.B")
    private void receiveFromB(String message){
        log.info("Message receive from queue B -> {}", message);
    }


    @RabbitListener(queues = "queue.All")
    private void receiveFromAll(String message){
        log.info("Message receive from queue All -> {}", message);
    }
*/


}
