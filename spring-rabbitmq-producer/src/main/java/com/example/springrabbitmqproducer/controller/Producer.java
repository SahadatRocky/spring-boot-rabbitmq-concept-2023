package com.example.springrabbitmqproducer.controller;

import com.example.springrabbitmqproducer.model.MyMessage;
//import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.HeadersExchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Producer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private DirectExchange exchange;

    @PostMapping("/post")
    public ResponseEntity<String> send(@RequestBody MyMessage message){
        rabbitTemplate.convertAndSend(exchange.getName(), "routing.A", message);
        return ResponseEntity.ok("Message received successfully");
    }

    /*
    @Autowired
    private FanoutExchange exchange;

    @PostMapping("/post")
    public ResponseEntity<String> send(@RequestBody MyMessage message){
        rabbitTemplate.convertAndSend(exchange.getName(),"", message);
        return ResponseEntity.ok("Message received successfully");
    }


     */

    /*
    @Autowired
    private TopicExchange exchange;

    @PostMapping("/post")
    public ResponseEntity<String> send(@RequestBody MyMessage message){
        rabbitTemplate.convertAndSend(exchange.getName(),"routing.A", message);
        return ResponseEntity.ok("Message received successfully");
    }

     */

    /*
    @Autowired
    private HeadersExchange exchange;

    @PostMapping("/post/{message}")
    public ResponseEntity<String> send(@PathVariable("message") String message){
        rabbitTemplate.convertAndSend("queue.A", message);
        return ResponseEntity.ok("Message received successfully");
    }

     */
}
