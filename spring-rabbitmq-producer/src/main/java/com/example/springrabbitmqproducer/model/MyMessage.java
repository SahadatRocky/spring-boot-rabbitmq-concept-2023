package com.example.springrabbitmqproducer.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MyMessage {

    private Integer id;
    private String name;
}
